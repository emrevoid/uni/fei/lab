# Snippets

[[_TOC_]]

## Esercitazione 01

### Esercizio 5: Negativo di un'immagine

#### Negativo di un'immagine grayscale

```csharp
Result = new LookupTableTransform<byte>(
                InputImage,	
                p => (Byte.MaxValue - p).ClipToByte()).Execute();
```

#### Negativo di un'immagine RGB

```csharp
PixelMapping<byte, byte> f = p => (Byte.MaxValue - p).ClipToByte();
Result = new RgbImage<byte>(new LookupTableTransform<byte>(InputImage.RedChannel, f).Execute(),
                new LookupTableTransform<byte>(InputImage.GreenChannel, f).Execute(),
                new LookupTableTransform<byte>(InputImage.BlueChannel, f).Execute());
```

Alternativamente è possibile usare la classe precedentemente implementata per il negativo di un'immagine,
ricordandosi però di creare un costruttore che assegni l'immagine.

```csharp
Result = new RgbImage<byte>(
        new NegativeImage(InputImage.RedChannel).Execute(),
        new NegativeImage(InputImage.GreenChannel).Execute(),
        new NegativeImage(InputImage.BlueChannel).Execute()
      );
```

### Esercizio 6: Variazione di luminosità

```csharp
Result = new LookupTableTransform<byte>(InputImage, p => (p + Variation * 255 / 100).ClipToByte()).Execute();
```

### Esercizio 7: da grayscale a pseudocolori

Si fa uso della classe `LookupTables` della libreria che mette a disposizione una LUT per lo spettro.

```csharp
Result = new RgbLookupTableTransform<byte>(InputImage, LookupTables.Spectrum).Execute();
```

## Esercitazione 02

### Esercizio 1: Operazioni sull'istogramma

- [x] Creazione istogramma
  ```csharp
  Result = new Histogram();
  foreach (var p in InputImage)
    Result[p]++;
  ```
- [x] Radice quadrata dell'istogramma
  ```csharp
  for (int i = 0; i < 256; i++)
    Result[i] = (int)Math.Round(Math.Sqrt((double)Result[i]));
  ```
- [x] Smoothing istogramma (media locale su un intorno del pixel)
  ```csharp
  Histogram histTmp = Result.Clone();
  for (int i = 0; i < 256; i++)
  {
    int j1 = Math.Max(0, i - SmoothWindowSize);
    int j2 = Math.Min(255, i + SmoothWindowSize);
    int n = 0;
    int sum = 0;
    for (int j = j1; j <= j2; j++, n++)
        sum += histTmp[j];
    Result[i] = sum / n;
  }
  ```

### Esercizio 2: Binarizzazione con soglia globale

```csharp
Result = new LookupTableTransform<byte>(InputImage,
					p => (byte)(p < this.Threshold ? 0 : 255)).Execute();
```

### Esercizio 3

#### Separa canali RGB

```csharp
R = InputImage.RedChannel.Clone();
G = InputImage.GreenChannel.Clone();
B = InputImage.BlueChannel.Clone();
```

#### Combina canali RGB

```csharp
this.Result = new RgbImage<byte>(Red, Green, Blue);
```

#### Variazione luminosità RGB

Si fa uso della classe implementata nell'[Esercitazione 1](https://gitlab.com/emrevoid/uni/fei/lab/-/blob/2020/famar/SNIPPET.md#esercizio-6-variazione-di-luminosit%C3%A0)

Si noti che la soluzione proposta (anche dal prof) non corrisponde alla richiesta dell'esercizio:

> Variare la luminosità di un’immagine RGB mediante un parametro che indica la variazione
> percentuale rispetto all’intervallo [0,255]

Definire un costruttore in `EditBrightness` può essere superfluo in quanto è possibile
usare una peculiarità di C# ed inizializzare l'oggetto assegnando allo stesso tempo
i valori delle proprietà. [Per informazioni](https://docs.microsoft.com/it-it/dotnet/csharp/programming-guide/classes-and-structs/how-to-initialize-objects-by-using-an-object-initializer)

```csharp
EditBrightness brEditor = new EditBrightness { Variazione = Variazione };
brEditor.InputImage = this.InputImage.RedChannel; var r = brEditor.Execute();
brEditor.InputImage = this.InputImage.GreenChannel; var g = brEditor.Execute();
brEditor.InputImage = this.InputImage.BlueChannel; var b = brEditor.Execute();
this.Result = new RgbImage<byte>(r, g, b);
```

#### RGB -> HSL

Viene fornita l'immagine RGB e vengono richieste le tre componenti H, S e L che sono di tipo `Image<byte>`

```csharp
var hslImage = InputImage.ToByteHslImage();
Hue = hslImage.HueChannel;
Saturation = hslImage.SaturationChannel;
Lightness = hslImage.LightnessChannel;
```

#### HSL -> RGB

Vengono fornite le tre componenti H, S e L separatamente.

```csharp
Result = new HslImage<byte>(this.Hue, this.Saturation, this.Lightness).ToByteRgbImage();
```

#### Grayscale -> RGB

Costituita da sfumature di uno stesso colore.

Vengono fornite le componenti H e S. L è sostanzialmente data dall'intensità dell'immagine di partenza.

```csharp
Result = new HslImage<byte>(
                new Image<byte>(InputImage.Width, InputImage.Height, Hue),
                new Image<byte>(InputImage.Width, InputImage.Height, Saturation),
                InputImage)
                .ToByteRgbImage();
```

## Esercitazione 03

### Esercizio 1: Contrast stretching istogramma

Formula: $`f(g)=255 \cdot \frac{g-min(g_{i})}{max(g_{i})-min(g_{i})}`$

Procedimento:
1. Calcolo del numero di pixel più chiari e più scuri
   (estremo sinistro e destro) dell'istogramma
2. Calcolo di min e max (indici dell'istogramma)
3. Calcolo della funzione di contrast stretching


```csharp
var hist = new PRLab.FEI.HistogramBuilder(InputImage).Execute();
// 1
int numPixelDiscard = InputImage.PixelCount * StretchDiscardPercentage / 100;

int min = 0, sum = 0;

// 2 left
while(sum < numPixelDiscard)
	sum += hist[min++];

sum = 0;
int max = Byte.MaxValue; // aka right

// 2 right
while (sum < numPixelDiscard)
	sum += hist[max--];

int diff = max - min;
diff = diff == 0 ? 1 : diff;

// 3
Result = new LookupTableTransform<byte>(
	InputImage,
	p => (255 * (p - min) / diff).ClipToByte())
	.Execute();
  ```

### Esercizio 2: Equalizzazione istogramma

Formula: $`f(g_{i}) = \frac{255}{nPixel} \cdot \sum_{j=0}^{i}{H[g_{j}]}`$

Procedimento:
1. Ricalcola ogni elemento dell'istogramma come somma dei precedenti (calcolo sommatoria)
2. Applica la formula

```csharp
var hist = new HistogramBuilder(InputImage).Execute();
for (int i = 1; i < Byte.MaxValue + 1; i++)
    hist[i] += hist[i - 1];

Result = new LookupTableTransform<byte>(InputImage,
	p => ((255 * hist[p]) / InputImage.PixelCount).ClipToByte()).Execute();
```

### Esercizio 3: Operazioni aritmetiche tra immagini grayscale

Vengono mostrate tutte le operazioni dell'enum `ImageArithmeticOperation`.

Nota:
- i risultati devono essere compresi nell'intervallo [0, 255]
- l'operazione di differenza calcola la differenza in valore assoluto
  (la sottrazione invece calcola la semplice differenza)

```csharp
var Result = new Image<byte>(InputImage1.Width, InputImage2.Height);
for (int i = 0; i < InputImage1.PixelCount; i++)
    Result[i] = (InputImage1[i] + InputImage2[i]).ClipToByte(); //somma
    Result[i] = (InputImage1[i] - InputImage2[i]).ClipToByte(); //sottrazione
    Result[i] = (Math.Abs(InputImage1[i] - InputImage2[i])).ClipToByte(); //differenza
    Result[i] = ((InputImage1[i] + InputImage2[i]) / 2).ClipToByte(); //media

    Result[i] = (InputImage1[i] & InputImage2[i]).ClipToByte(); //and
    Result[i] = (InputImage1[i] | InputImage2[i]).ClipToByte(); //or
    Result[i] = (InputImage1[i] ^ InputImage2[i]).ClipToByte(); //xor

    Result[i] = Math.Min(InputImage1[i],InputImage2[i]); //darkest
    Result[i] = Math.Max(InputImage1[i], InputImage2[i]); //lightest
```

### Esercizio 4:  Operazioni aritmetiche tra immagini RGB

Riutilizzo della classe creata nell'esercizio precedente:

```csharp
var r = new ImageArithmetic(InputImage1.RedChannel.Clone(),
			    InputImage2.RedChannel.Clone(),
			    Operation)
			    .Execute();
// allo stesso modo per i canali green e blue
Result = new RgbImage<byte>(r, g, b);
```

## Esercitazione 04

### Esercizio 1: Trasformazione affine grayscale

Rivediamo [la formula del Mapping inverso](https://gitlab.com/emrevoid/uni/fei/course/-/blob/master/01_OPERAZIONI_IMMAGINI.md#ruotare-e-ridimensionare-unimmagine):

```math
\left[\begin{matrix} x_{old} \\ y_{old} \end{matrix} \right] =
\left[\begin{matrix} \frac{1}{s} & 0 \\ 0 & \frac{1}{s} \end{matrix} \right] \cdot
\left[\begin{matrix} cos(-\theta) & sin(-\theta) \\ -sin(-\theta) & cos(-\theta) \end{matrix} \right] \cdot
\left(
\left[\begin{matrix} x_{new} \\ y_{new} \end{matrix} \right] -
\left[\begin{matrix} t_{x} \\ t_{y} \end{matrix} \right]
\right)
```

- `prepareMapping`
    - inverso di `s` (scala)
        ```csharp
        InvSx = 1.0 / ScaleFactorX;
        InvSy = 1.0 / ScaleFactorY;
        ```
    - calcolo del seno e del coseno dell'angolo
        ```csharp
        double effectiveRotationAngle = -RotationDegrees; // Si cambia segno all'angolo per invertire il senso di rotazione
        double minusThetaRad = -Math.PI * effectiveRotationAngle / 180;
        SinMinusTheta = Math.Sin(minusThetaRad);
        CosMinusTheta = Math.Cos(minusThetaRad);
        ```
    - calcolo di tx e ty
        ```csharp
        // (CxPixelsInputImage, CyPixelsInputImage): punto centrale dell'immagine di partenza (con RotationCenterX e Y di default a 0.5)
        CxPixelsInputImage = RotationCenterX * InputImage.Width;
        CyPixelsInputImage = RotationCenterY * InputImage.Height;

        // (cxPixels, cyPixels): punto centrale dell'immagine di destinazione (con RotationCenterX e Y di default a 0.5)
        var cxPixels = RotationCenterX * Result.Width;
        var cyPixels = RotationCenterY * Result.Height;


        CTx = cxPixels + TranslationX;
        CTy = cyPixels + TranslationY;
        ```
- per ogni y e per ogni x
    - chiamata metodo `Map`
        - effettiva operazione di mapping
          ```csharp
          // Mapping
          double a = (double)x - CTx; // xNew - tx
          double b = (double)y - CTy; // yNew - ty

          // Prodotto tra matrici
          double xOld = InvSx * CosMinusTheta * a + InvSy * SinMinusTheta * b + CxPixelsInputImage; // + CxPixelsInputImage: successiva traslazione con riferimento al punto centrale dell'immagine di partenza
          double yOld = InvSx * -SinMinusTheta * a + InvSy * CosMinusTheta * b + CyPixelsInputImage;
          ```
        - si calcolano i pesi wA, wB, wC e wD
            ```csharp
            // Calcolo dei pesi

            xL = (int)Math.Floor(xOld); // perché xL assume tale valore?
            yL = (int)Math.Floor(yOld);

            // come nelle slide
            wA = (xL + 1 - xOld) * (yL + 1 - yOld);
            wB = (xOld - xL) * (yL + 1 - yOld);
            wC = (xL + 1 - xOld) * (yOld - yL);
            wD = (xOld - xL) * (yOld - yL);
            ```
    - quindi viene effettuata l'**interpolazione di Lagrange**:
        - calcolo dei punti A, B, C e D
            ```csharp
            // se siamo all'interno delle dimensioni dell'immagine originale
            if ((x1 >= 0) && (x1 < width - 1) && (y1 >= 0) && (y1 < height - 1))
            {
                int index = x1 + y1 * width;
                // Gli altri 3 pixel sono adiacenti
                A = InputImage[index];
                B = InputImage[index + 1];
                C = InputImage[index + width];
                D = InputImage[index + width + 1];
            }
            else
            {
                // Li controlla individualmente
                A = ImgOrBack(x1, y1);
                B = ImgOrBack(x1 + 1, y1);
                C = ImgOrBack(x1, y1 + 1);
                D = ImgOrBack(x1 + 1, y1 + 1);

                /*
                    Implementazione di ImgOrBack
                    private byte ImgOrBack(int x, int y)
                    {
                        if (x >= 0 && x < InputImage.Width && y >= 0 && y < InputImage.Height)
                            return InputImage[y, x];
                        else return Background;
                    }
                */
            }
            Result[i++] = Interpolate(A, B, C, D);
            ```


$`I(x_{new}, y_{new})`$ viene quindi calcolato (dalla chiamata al metodo `Interpolate`) come segue:

**NB: la divisione per wA + wB + wC + wD non viene fatta poiché, come mostrato sulle slide, tale valore è 1**

```csharp
double value = wA * A + wB * B + wC * C + wD * D;
```

`value` viene quindi arrotondato e fatto il casting a byte con l'istruzione `ImageUtilities.RoundAndClipToByte(value);`.

### Esercizio 2: Trasformazione affine RGB

Come la trasformazione affine Grayscale ma l'interpolazione viene fatta sui tre canali dei quattro `RgbPixel` A, B, C e D.

### Esercizio 3: Trasformazione affine RGB (efficiente)

TODO

## Esercitazione 05

### Esercizio 1: Convoluzione

#### Implementazione di base

Riprendendo il codice dalle slide (segue spiegazione):

```csharp
Result = new Image<int>(InputImage.Width, InputImage.Height);
int w = InputImage.Width;
int h = InputImage.Height;
int m = Filter.Size;
int m2 = m / 2;
int i1 = m2;
int i2 = h - m2 - 1;
int j1 = m2;
int j2 = w - m2 - 1;

// I bordi in Result restano a 0
for (int i = i1; i <= i2; i++)
    for (int j = j1; j <= j2; j++)
    {
        int val = 0;
        for (int y = 0; y < m; y++)
            for (int x = 0; x < m; x++)
                val += InputImage[i + m2 - y, j + m2 - x] * Filter[y, x];
        Result[i, j] = val / Filter.Denominator;
    }

```

Spiegazione:
- i due cicli `for` scandiscono tutta l'immagine (perché `i2` e `j2`, che sono i valori limiti dei cicli, corrispondono rispettivamente
ad altezza e larghezza dell'immagine esclusi i bordi di dimensione m/2 + 1)
- i due cicli `for` più interni scandiscono il filtro e calcolo il valore del pixel (che dovrà poi essere diviso per `Filter.Denominator`)

#### con filtro linearizzato e offset precalcolati

Procedimento:
1. Calcolo array offset e array valori del filtro linearizzato
2. Codifica dell'algoritmo di applicazione del filtro

```csharp
// 1
int[] LinearFilterOffsets;
int[] LinearFilterValues = Filter.CalculateConvolutionValuesAndOffsets(InputImage.Width, out LinearFilterOffsets);

// 2
Result = new Image<int>(w, h);

// Computing the convolutional operation
for(int y = y1; y <= y2; y++, index += indexStepRow)
    for(int x = x1; x <= x2; x++)
    {
        int val = 0;
        for (int k = 0; k < LinearFilterValues.Length; k++)
            val += InputImage[index + LinearFilterOffsets[k]] * LinearFilterValues[k];
        Result[index++] = val / Filter.Denominator;
    }
```

Codice preso dalle slide (segue spiegazione):

```csharp
int nM = Filter.Size * Filter.Size;
int[] FOff = new int[nM];
int[] FVal = new int[nM];
int maskLen = 0;
for (int y = 0; y < Filter.Size; y++)
    for (int x = 0; x < Filter.Size; x++)
        if (Filter[y, x] != 0)
        {
            FOff[maskLen] = (m2 - y) * w + (m2 - x);
            FVal[maskLen] = Filter[y, x];
            maskLen++;
        }
int index = m2 * (w + 1);
// indice lineare all'interno dell'immagine
int indexStepRow = m2 * 2; // aggiustamento indice a fine riga (salta bordi)
for (int y = y1; y <= y2; y++, index += indexStepRow)
    for (int x = x1; x <= x2; x++)
    {
        int val = 0;
        for (int k = 0; k < maskLen; k++)
            val += InputImage[index + FOff[k]] * FVal[k];
        Result[index++] = val / Filter.Denominator;
    }
```

Spiegazione:
- i primi due cicli for servono per calcolare e memorizzare, scorrendo `Filter`, gli offset e i valori del filtro diversi da zero
- x1 è i1, x2 è i2, y1 è j1, y2 è j2
- il for più interno scorre il filtro linearizzato
- la variabile `k` viene appunto usata per scorrere gli array per gli offset e per i valori del filtro diversi da zero

## Esercizio 2: Filtro di smoothing

```csharp
				     // size, denominatore
var filter = new ConvolutionFilter<int>(Size, Size * Size);
for(int i = 0; i < filter.Size; i++)
    filter[i] = 1;

Result = new ConvoluzioneByteInt(this.InputImage, filter)
				.Execute()
				.ToByteImage(PixelConversionMethod.Clip);
```

## Esercizio 3: Filtro di sharpening

```csharp
var filter = new ConvolutionFilter<int>(new int[] { -1, -1, -1,
						    -1,  8, -1,
						    -1, -1, -1},
					1);
var tmp = new ConvoluzioneByteInt(this.InputImage, filter).Execute();
// Result = Image + Weight * FilterResult
Result = this.InputImage.Clone();
for(int i = 0; i < Result.PixelCount; i++)
    Result[i] = (this.InputImage[i] + tmp[i] * weight).RoundAndClipToByte();
```

## Esercizio 4

Esercizio di GUI

## Esercitazione 06

### Esercizio 1: Modulo gradiente

Riutilizzo del filtro linearizzato creato nell'esercitazione 05

```csharp
Result = new Image<int>(this.InputImage.Width, this.InputImage.Height);
var gradX = new ConvoluzioneByteInt(this.InputImage, new ConvolutionFilter<int>(new int[]
						       {1, 0, -1,
							1, 0, -1,
							1, 0, -1}, 3)).Execute();
var gradY = new ConvoluzioneByteInt(this.InputImage, new ConvolutionFilter<int>(new int[]
						       {1, 1, 1,
							0, 0, 0,
							-1, -1, -1}, 3)).Execute();
for(int i = 0; i < this.InputImage.PixelCount; i++)
	Result[i] = (int)Math.Round(Math.Sqrt(
				Math.Pow(gradX[i], 2) + 
				Math.Pow(gradY[i], 2)), 
			MidpointRounding.AwayFromZero);
```

### Esercizio 2: Modulo gradiente con risultati intermedi

sembra un esercizio di GUI...

### Esercizio 3: Trasformata distanza

con utilizzo del cursore

#### Metrica d4

```csharp
this.Result = new Image<int>(this.InputImage.Width, this.InputImage.Height);
var cursor = new ImageCursor(this.Result, 1);

if(Metric == MetricType.CityBlock)
{
    // scansione diretta
    do
    {
        if(this.InputImage[cursor] == Foreground)
            Result[cursor] = Math.Min(Result[cursor.North], Result[cursor.West]) + 1;
    } while (cursor.MoveNext());

    // scansione inversa
    do
    {
        if(this.InputImage[cursor] == Foreground)
            Result[cursor] = Math.Min(Math.Min(
					Result[cursor.South] + 1,
					Result[cursor.East] + 1),
				      Result[cursor]);
    } while (cursor.MovePrevious());
}
```

#### Metrica d8

```csharp
else if (Metric == MetricType.Chessboard)
{
    // scansione diretta
    do
    {
        if (this.InputImage[cursor] == Foreground)
            Result[cursor] = Math.Min(
				Math.Min(Result[cursor.Northwest], Result[cursor.North]),
				Math.Min(Result[cursor.Northeast], Result[cursor.West]))
				+ 1;
    } while (cursor.MoveNext());

    // scansione inversa
    do
    {
        if (this.InputImage[cursor] == Foreground)
            Result[cursor] = Math.Min(Math.Min(
				Math.Min(Result[cursor.Southeast] + 1, Result[cursor.South] + 1),
				Math.Min(Result[cursor.Southwest] + 1, Result[cursor.East] + 1)),
			     Result[cursor]);
    } while (cursor.MovePrevious());
}
```

## Esercitazione 07

### Esercizio 1: Estrazione bordi

Procedimento (considerando una metrica d4 per il foreground):
1. scorro con un cursore dall'inizio dell'immagine fino a trovare un pixel di foreground
2. imposto la direzione di provenienza (ovest perché la scansione è row-wise)
3. creazione della struttura dati atta a memorizzare il contorno
4. ricerca della prossima direzione da visitare (se è un pixel di foreground)
5. aggiungo la direzione alla catena delle direzioni (costituisce il contorno)
6. sposto un nuovo cursore nella direzione trovata
7. calcolo la direzione di provenienza come opposto della direzione trovata
8. ritorna al punto 4) finché il nuovo cursore non è uguale al pixel di partenza

```csharp
// 1
var pixelStart = new ImageCursor(this.InputImage);
while (pixelStart.MoveNext() && pixelStart != Foreground) { }
// 2
CityBlockDirection direction = CityBlockDirection.West;
// 3
var contour = new CityBlockContour(pixelStart.X, pixelStart.Y);
var cursor = new ImageCursor(pixelStart);
do
{
    // 4
    for(int i = 0; i < 4; i++)
    {
        direction = CityBlockMetric.GetNextDirection(direction);
        if(this.InputImage[cursor.GetAt(direction)] == Foreground)
			break;
    }

    contour.Add(direction); // 5
    cursor.MoveTo(direction); // 6
    direction = CityBlockMetric.GetOppositeDirection(direction); // 7
} while (cursor != pixelStart); // 8
```

### Esercizio 2: Estrazione di tutti i bordi

Variante dell'esercizio 1, che permette di estrarre i contorni di tutti gli oggetti presenti nell'immagine di input.

- memorizzo il background (negazione bitwise del Foreground)
    ```csharp
    byte background = (byte)~Foreground;
    ```
- lavoro su un'immagine copia che esclude il bordo
    ```csharp
    Image<byte> workImage = (Image<byte>)new ImageBorderFilling<byte>(InputImage, 1, background, false).Execute();
    ```
- definisco un array booleano che, per ogni pixel, mi dirà se è da visitare (è di contorno)
    ```csharp
    bool[] toBeVisited = new bool[workImage.PixelCount];
    ```
- marco tutti i pixel di bordo come da visitare (nell'if controlla che il pixel corrente sia di foreground, che almeno uno dei vicini sia background e che almeno uno dei vicini sia foreground; se l'if passa, il pixel viene segnato come da visitare, quindi è di bordo)
    ```csharp
    ImageCursor pixelCursor = new ImageCursor(workImage);
    do
    {
        // Utilizza metrica d4 e cerca pixel di bordo che non siano isolati
        if (workImage[pixelCursor] == Foreground &&
            (workImage[pixelCursor.North] != Foreground || workImage[pixelCursor.East] != Foreground ||
                workImage[pixelCursor.West] != Foreground || workImage[pixelCursor.South] != Foreground) &&
            (workImage[pixelCursor.North] == Foreground || workImage[pixelCursor.East] == Foreground ||
                workImage[pixelCursor.West] == Foreground || workImage[pixelCursor.South] == Foreground))
            toBeVisited[pixelCursor] = true;
    } while (pixelCursor.MoveNext());
    ```
- inizializzazione della struttura dati ovvero la lista di contorni
    ```csharp
    Result = new List<CityBlockContour>();
    ```
- scorre tutti i pixel da visitare
  - per ognuno di essi, segue il contorno
  - ottenuto il contorno, lo aggiunge alla lista dei contorni degli oggetti
    ```csharp
      pixelCursor.Restart();
      do
      {
        if (toBeVisited[pixelCursor])
        {
          CityBlockContour contour = new CityBlockContour(pixelCursor.X, pixelCursor.Y);
          FollowContour(workImage, pixelCursor, contour, toBeVisited);
          Result.Add(contour);
        }
      } while (pixelCursor.MoveNext());
    ```
- implementazione di `FollowContour`
  - Ricava la direzione di "entrata", cercando un pixel di background vicino
      ```csharp
      CityBlockDirection direction;
      for (direction = CityBlockDirection.East; direction <= CityBlockDirection.South; direction++)
      {
        if (image[cursor.GetAt(direction)] != Foreground)
          break;
      }
      ```
  - per ogni pixel
    - marca il pixel corrente come da non visitare (non più)
    - cerca la nuova direzione da aggiungere al contorno (segue il bordo)
    - **aggiunge la nuova direzione alla lista dei contorni**
    - segue la nuova direzione spostando il cursore nella posizione indicata
    - marca la direzione come opposta a quella di ingresso (a cosa serve?)
    - si ferma quando incontra il pixel iniziale
    ```csharp
    do
    {
        toBeVisited[cursor] = false;
        for (int j = 1; j <= 4; j++)
        {
          direction = CityBlockMetric.GetNextDirection(direction);
          if (image[cursor.GetAt(direction)] == Foreground)
          {
            break;
          }
        }
        contour.Add(direction); // nuova direzione aggiunta al contorno
        cursor.MoveTo(direction); // si sposta seguendo la direzione
        direction = CityBlockMetric.GetOppositeDirection(direction);  // direzione rispetto al pixel precedente
    } while (cursor != pixelStart);    // si ferma quando incontra il pixel iniziale
    ```

## Esercitazione 08

### Esercizio 1: Etichettatura componenti connesse

Stessa implementazione delle dispense (topologia digitale, pag. 21)

### Esercizio 3: informazioni componenti connesse

- numero componenti connesse
  ```csharp
  ConnectedComponents = et.Execute();
  this.ConnectedComponentsCount = ConnectedComponents.ComponentCount;
  ```
- Area minima, area massima, area media delle componenti connesse
  ```csharp
  this.Areas = new int[this.ConnectedComponentsCount];
  foreach (var pixel in ConnectedComponents)
  	if(pixel >= 0)
  		Areas[pixel]++;

  this.MinArea = Areas.Min();
  this.MaxArea = Areas.Max();
  this.AvgArea = Areas.Average();
  ```
- media perimetro componenti connesse
  ```csharp
  int[] perimeters = new int[this.ConnectedComponentsCount];
  var cursor = new ImageCursor(ConnectedComponents, 1);
  do
  {
      var pixel = ConnectedComponents[cursor];
      if (pixel >= 0)
          for(var dir = ChessboardDirection.East; dir <= ChessboardDirection.Southeast; dir++)
              if(ConnectedComponents[cursor.GetAt(dir)] < 0)
              {
                  perimeters[pixel]++;
                  break;
              }
  } while (cursor.MoveNext());
  this.AvgPerimeter = perimeters.Average();
  ```

### Esercizio 4: Elimina piccole componenti

```csharp
var background = (byte)(Byte.MaxValue - this.Foreground);

for(int i = 0; i < this.InputImage.PixelCount; i++)
{
    var cc = ComponentiConnesseInfo.ConnectedComponents[i];
    if(cc >= 0 && ComponentiConnesseInfo.Areas[cc] < this.AreaMinima)
        Result[i] = background;
}
```

## Esercitazione 09

### Esercizio 1: Dilatazione

```csharp
Result = new Image<byte>(InputImage.Width, InputImage.Height, (byte)(255 - Foreground));

// Costruisce l'array degli offset dall'elemento strutturante riflesso
int[] elementOffsets = MorphologyStructuringElement.CreateOffsets(StructuringElement, InputImage, true);

// Crea un cursore per scorrere l'immagine escludendo i pixel di bordo
var pixelCursor = new ImageCursor(StructuringElement.Width / 2, StructuringElement.Height / 2, InputImage.Width - 1 - StructuringElement.Width / 2, InputImage.Height - 1 - StructuringElement.Height / 2, InputImage);
do
{
    foreach (int offset in elementOffsets)
    {
        if (InputImage[pixelCursor + offset] == Foreground)
        {
            Result[pixelCursor] = Foreground;
            break; // esce dal foreach
        }
    }
} while (pixelCursor.MoveNext());
```

### Esercizio 2: Erosione

```csharp
byte background = (byte)(255 - Foreground);
Result = new Image<byte>(InputImage.Width, InputImage.Height, background);

// Costruisce l'array degli offset dall'elemento strutturante
int[] elemOffsets = MorphologyStructuringElement.CreateOffsets(StructuringElement, InputImage, false);

// Crea un cursore per scorrere l'immagine escludendo i pixel di bordo
ImageCursor pixelCursor = new ImageCursor(StructuringElement.Width / 2, StructuringElement.Height / 2, InputImage.Width - 1 - StructuringElement.Width / 2, InputImage.Height - 1 - StructuringElement.Height / 2, InputImage);
do
{
    byte pixelValue = Foreground;
    foreach (int offset in elemOffsets)
    {
        if (InputImage[pixelCursor + offset] != Foreground)
        {
            pixelValue = background;
            break; // esce dal foreach
        }
    }
    Result[pixelCursor] = pixelValue;
} while (pixelCursor.MoveNext());
```

### Esercizio 3: Apertura e Chisura

Apertura

```csharp
var erosion = new MorphologyErosion(InputImage, StructuringElement, Foreground);
var dilation = new MorphologyDilation(erosion.Execute(), StructuringElement, Foreground);
Result = dilation.Execute();
```

Chiusura

```csharp
MorphologyDilation dilation = new MorphologyDilation(InputImage, StructuringElement, Foreground);
MorphologyErosion erosion = new MorphologyErosion(dilation.Execute(), StructuringElement, Foreground);
Result = erosion.Execute();
```

### Esercizio 4: Estrazione del bordo mediante morfologia

```csharp
var erosion = new MorphologyErosion(InputImage, StructuringElement, Foreground);
```

### Esercizio 4: Estrazione del bordo mediante morfologia (famar)

```csharp
var erosion = new MorphologyErosion(InputImage, StructuringElement, Foreground).Execute();
for (int i = 0; i < InputImage.PixelCount; i++)
{
    Result[i] = InputImage[i] - erosion[i];
}
```

### Esercizio 4: Hit-or-Miss Transform (famar)

Vanno usati due elementi strutturanti

- complementare di F
    ```csharp
    Image<byte> Fc = new Image<byte>(InputImage.Width, InputImage.Height)
    for (int i = 0; i < InputImage.PixelCount; i++) // ?
    {
        if (InputImage[i] != 255)
        {
            Fc[i] = 255;
        }
    }
    ```
