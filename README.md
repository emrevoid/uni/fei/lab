﻿# FEI Laboratory

## Content

- PRLab project
- BioLab library
- Example images
- [Snippets / Solutions](SNIPPET.md)

## Instructions

- Clone this repo: `git clone git@gitlab.com:emrevoid/uni/fei/lab.git fei-lab`
- Checkout to your branch
- Open Visual Studio
- Click "Open a project or solution"
- Select `fei-lab/PRLab.sln` file
- Wait until the project is fully loaded
- Go to "Build" and select "Clean Solution"
- Go to "Build" and select "Build Solution"
- Click on "Start"
- Open an image
- Go to "Esercitazioni FEI" and select some operations to check if it works
