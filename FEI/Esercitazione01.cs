﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;

namespace PRLab.FEI
{

  [AlgorithmInfo("Negativo Grayscale", Category = "FEI")]
  public class NegativeImage : ImageOperation<Image<byte>,Image<byte>>
  {
    public override void Run()
    {
      Result = new LookupTableTransform<byte>(InputImage, p => (Byte.MaxValue - p).ClipToByte()).Execute();
    }
  }

  [AlgorithmInfo("Negativo RGB", Category = "FEI")]
  public class NegativeRgbImage : ImageOperation<RgbImage<byte>, RgbImage<byte>>
  {
    public override void Run()
    {
      // Non è possibile usare RgbLookupTable perché
      // This class executes a look up table transformation with an rgb image as output.
      PixelMapping<byte, byte> f = p => (Byte.MaxValue - p).ClipToByte();
      Result = new RgbImage<byte>(new LookupTableTransform<byte>(InputImage.RedChannel, f).Execute(),
                                  new LookupTableTransform<byte>(InputImage.GreenChannel, f).Execute(),
                                  new LookupTableTransform<byte>(InputImage.BlueChannel, f).Execute());
    }
  }

  [AlgorithmInfo("Variazione luminosità grayscale", Category = "FEI")]
  public class EditBrightness : ImageOperation<Image<byte>, Image<byte>>
  {
    private int variazione = 0;
    
    [AlgorithmParameter]
    public int Variazione {
      get {
        return variazione;
      }
      set {
        if (value < -100 || value > 100)
		{
          throw new ArgumentOutOfRangeException("Value must be between -100 and 100");
		}

        variazione = value;
      }
    }

    public override void Run()
    {
      Result = new LookupTableTransform<byte>(InputImage, p => (p + variazione * Byte.MaxValue / 100).ClipToByte()).Execute();
    }
  }

  [AlgorithmInfo("Grayscale -> pseudocolori", Category = "FEI")]
  public class GrayscaleToPseudocolors : ImageOperation<Image<byte>, RgbImage<byte>>
  {
    public override void Run()
    {
      Result = new RgbImage<byte>(InputImage.Width, InputImage.Height);
      for (int i = 0; i < InputImage.PixelCount; i++)
	  {
        var lut = LookupTables.Spectrum.ToArray();
        Result[i] = lut[InputImage[i]];
	  }
    }
  }
}
