﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using System.Reflection;

namespace PRLab.FEI
{

  [AlgorithmInfo("Stretch del contrasto", Category = "FEI")]
  public class ContrastStretch : ImageOperation<Image<byte>, Image<byte>>
  {
    private int stretchDiscardPerc;

    [AlgorithmParameter]
    [DefaultValue(0)]
    public int StretchDiscardPercentage
    {
      get { return stretchDiscardPerc; }
      set
      {
        if (value < 0 || value > 100)
          throw new ArgumentOutOfRangeException("Inserire un valore fra 0 and 100.");
        stretchDiscardPerc = value;
      }
    }

    public ContrastStretch()
    {
    }

    public ContrastStretch(Image<byte> inputImage)
      : base(inputImage)
    {
      StretchDiscardPercentage = 0;
    }

    public ContrastStretch(Image<byte> inputImage, int stretchDiscard)
      : base(inputImage)
    {
      StretchDiscardPercentage = stretchDiscard;
    }

    public override void Run()
    {
      // Perché non usa .FindMax() ?

      Histogram hist = new HistogramBuilder(InputImage).Execute();

      int numToDiscard = InputImage.PixelCount * StretchDiscardPercentage / 100;

      int sum = 0;
      int i = 0;
      while (sum < numToDiscard)
      {
        sum += hist[i++];
      }

      int j = 255;
      sum = 0;
      while (sum < numToDiscard)
      {
        sum += hist[j--];
      }

      int diff = j - i;
      if (diff > 0)
      {
        var op = new LookupTableTransform<byte>(InputImage,
                  p => (255 * (p - i) / diff).ClipToByte());
        Result = op.Execute();
      }
      else Result = InputImage.Clone();
    }
  }

  [AlgorithmInfo("Equalizzazione istogramma", Category = "FEI")]
  public class HistogramEqualization : ImageOperation<Image<byte>, Image<byte>>
  {
    public HistogramEqualization()
    {
    }

    public HistogramEqualization(Image<byte> inputImage)
      : base(inputImage)
    {
    }

    public override void Run()
    {
      var hist = new HistogramBuilder(InputImage).Execute();
      // Ricalcola ogni elemento dell'istogramma come somma dei precedenti
      for (int i = 1; i < 256; i++)
        hist[i] += hist[i - 1];
      // Definisce la funzione di mapping e applica la LUT
      var op = new LookupTableTransform<byte>(InputImage,
                  p => (byte)(255 * hist[p] / InputImage.PixelCount));
      Result = op.Execute();
    }
  }

  [AlgorithmInfo("Operazione aritmetica", Category = "FEI")]
  public class ImageArithmetic : ImageOperation<Image<byte>, Image<byte>, Image<byte>>
  {
    [AlgorithmParameter]
    [DefaultValue(defaultOperation)]
    public ImageArithmeticOperation Operation { get; set; }
    const ImageArithmeticOperation defaultOperation = ImageArithmeticOperation.Difference;

    public ImageArithmetic()
    {
    }

    public ImageArithmetic(Image<byte> image1, Image<byte> image2, ImageArithmeticOperation operation)
      : base(image1, image2)
    {
      Operation = operation;
    }

    public ImageArithmetic(Image<byte> image1, Image<byte> image2)
      : this(image1, image2, defaultOperation)
    {
    }

    public override void Run()
    {
      Result = new Image<byte>(InputImage1.Width, InputImage1.Height);
      if (Operation == ImageArithmeticOperation.Add)
      {
        for (int i = 0; i < InputImage1.PixelCount; i++)
          Result[i] = (byte)(InputImage1[i] + InputImage2[i]);
      }
      else if (Operation == ImageArithmeticOperation.And)
      {
        for (int i = 0; i < InputImage1.PixelCount; i++)
          Result[i] = (byte)(InputImage1[i] & InputImage2[i]);
      }
      else if (Operation == ImageArithmeticOperation.Difference)
      {
        for (int i = 0; i < InputImage1.PixelCount; i++)
          Result[i] = (byte)Math.Abs(InputImage1[i] - InputImage2[i]);
      }
      else if (Operation == ImageArithmeticOperation.Or)
      {
        for (int i = 0; i < InputImage1.PixelCount; i++)
          Result[i] = (byte)(InputImage1[i] | InputImage2[i]);
      }
      else if (Operation == ImageArithmeticOperation.Subtract)
      {
        for (int i = 0; i < InputImage1.PixelCount; i++)
          Result[i] = (byte)(InputImage1[i] - InputImage2[i]);
      }
      else if (Operation == ImageArithmeticOperation.Xor)
      {
        for (int i = 0; i < InputImage1.PixelCount; i++)
          Result[i] = (byte)(InputImage1[i] ^ InputImage2[i]);
      }
      else if (Operation == ImageArithmeticOperation.Average)
      {
        for (int i = 0; i < InputImage1.PixelCount; i++)
          Result[i] = (byte)(InputImage1[i] + InputImage2[i] / 2);
      }
      else if (Operation == ImageArithmeticOperation.Darkest)
      {
        for (int i = 0; i < InputImage1.PixelCount; i++)
          Result[i] = (byte)Math.Min(InputImage1[i], InputImage2[i]);
      }
      else if (Operation == ImageArithmeticOperation.Lightest)
      {
        for (int i = 0; i < InputImage1.PixelCount; i++)
          Result[i] = (byte)Math.Max(InputImage1[i], InputImage2[i]);
      }
    }
  }
}
