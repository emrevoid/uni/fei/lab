﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI
{

  [AlgorithmInfo("Calcolo Istogramma", Category = "FEI")]
  public class HistogramBuilder : ImageOperation<Image<byte>, Histogram>
  {
    [AlgorithmParameter]
    [DefaultValue(false)]
    public bool Sqrt { get; set; }

    [AlgorithmParameter]
    [DefaultValue(0)]
    public int SmoothWindowSize { get; set; }

    public HistogramBuilder(Image<byte> inputImage)
    {
      InputImage = inputImage;
    }

    public HistogramBuilder(Image<byte> inputImage, bool sqrt, int smoothWindowSize)
    {
      InputImage = inputImage;
      Sqrt = sqrt;
      SmoothWindowSize = smoothWindowSize;
    }

    public override void Run()
    {
      Result = new Histogram();
      foreach (var p in InputImage)
      {
        Result[p]++;
      }

      if (Sqrt)
      {
        for (int i = 0; i < Result.Count(); i++)
        {
          Result[i] = (int)Math.Round(Math.Sqrt(Result[i]));
        }
      }

      // ogni elemento è ricalcolato come media locale sull’intorno di 2 * smoothSize + 1 elementi
      if (SmoothWindowSize > 0)
      {
        Histogram histTmp = Result.Clone();
        for (int i = 0; i < 256; i++)
        {
          int j1 = Math.Max(0, i - SmoothWindowSize);
          int j2 = Math.Min(255, i + SmoothWindowSize);
          int n = 0;
          int sum = 0;
          for (int j = j1; j <= j2; j++, n++)
            sum += histTmp[j];
          Result[i] = sum / n;
        }
      }
    }
  }

  /*
   * Dalla documentazione di BioLab:
   * This image operation performs a thresholding operation on a grayscale image.
   * The output is a binary image where black pixels correspond to background and white pixels correspond to foreground (or vice versa).
   * The segmentation is determined by a single parameter (the intensity threshold).
   * In a single pass, each pixel in the image is compared with this threshold.
   * If the pixel's intensity is higher than the threshold, the pixel is set to white in the output;
   *  if it is less than the threshold, it is set to black.
   */
  [AlgorithmInfo("Binarizzazione", Category = "FEI")]
  public class Binarization : ImageOperation<Image<byte>, Image<byte>>
  {
    [AlgorithmParameter]
    public byte Threshold { get; set; } = 0;

    public override void Run()
    {
      Result = new LookupTableTransform<byte>(InputImage, p => p > Threshold ? Byte.MaxValue : Byte.MinValue).Execute();
    }
  }

  [AlgorithmInfo("Separa canali RGB", Category = "FEI")]
  public class SeparateRGB : Algorithm
  {
    [AlgorithmInput]
    public RgbImage<byte> InputImage { get; set; }

    [AlgorithmOutput]
    public Image<byte> R { get; set; }

    [AlgorithmOutput]
    public Image<byte> G { get; set; }

    [AlgorithmOutput]
    public Image<byte> B { get; set; }

    public override void Run()
    {
      R = InputImage.RedChannel.Clone();
      G = InputImage.GreenChannel.Clone();
      B = InputImage.BlueChannel.Clone();
    }
  }

  [AlgorithmInfo("Combina canali RGB", Category = "FEI")]
  public class CombineRGB : Algorithm
  {
    [AlgorithmOutput]
    public RgbImage<byte> Result { get; set; }

    [AlgorithmInput]
    public Image<byte> R { get; set; }

    [AlgorithmInput]
    public Image<byte> G { get; set; }

    [AlgorithmInput]
    public Image<byte> B { get; set; }

    public override void Run()
    {
      Result = new RgbImage<byte>(R.Clone(), G.Clone(), B.Clone());
    }

    [AlgorithmInfo("Variazione luminosità RGB", Category = "FEI")]
    public class BrightnessRGB : ImageOperation<RgbImage<byte>, RgbImage<byte>>
    {
      private int variazione;

      [AlgorithmParameter]
      public int Variazione
      {
        get
        {
          return variazione;
        }
        set
        {
          if (value < -100 || value > 100)
          {
            throw new ArgumentOutOfRangeException("Value must be between -100 and 100");
          }

          variazione = value;
        }
      }

      public override void Run()
      {
        EditBrightness eb = new EditBrightness { Variazione = Variazione };

        eb.InputImage = InputImage.RedChannel;
        var r = eb.Execute();
        eb.InputImage = InputImage.GreenChannel;
        var g = eb.Execute();
        eb.InputImage = InputImage.BlueChannel;
        var b = eb.Execute();

        Result = new RgbImage<byte>(r, g, b);
      }
    }
  }

  [AlgorithmInfo("RGB -> HSL", Category = "FEI")]
  public class RGBtoHSL : Algorithm
  {
    [AlgorithmInput]
    public RgbImage<byte> InputImage { get; set; }

    [AlgorithmOutput]
    public Image<byte> Hue { get; set; }

    [AlgorithmOutput]
    public Image<byte> Saturation { get; set; }

    [AlgorithmOutput]
    public Image<byte> Lightness { get; set; }
    
    public override void Run()
    {
      var hsl = InputImage.ToByteHslImage();
      Hue = hsl.HueChannel.Clone();
      Saturation = hsl.SaturationChannel.Clone();
      Lightness = hsl.LightnessChannel.Clone();
    }
  }

  [AlgorithmInfo("HSL -> RGB", Category = "FEI")]
  public class HSLtoRGB : Algorithm
  {
    [AlgorithmInput]
    public Image<byte> Hue { get; set; }

    [AlgorithmInput]
    public Image<byte> Saturation { get; set; }

    [AlgorithmInput]
    public Image<byte> Lightness { get; set; }

    [AlgorithmOutput]
    public RgbImage<byte> Result { get; set; }

    public override void Run()
    {
      Result = new HslImage<byte>(Hue, Saturation, Lightness).ToByteRgbImage();
    }
  }

  [AlgorithmInfo("Grayscale -> RGB", Category = "FEI")]
  public class GrayscaleToRGB : ImageOperation<Image<byte>, RgbImage<byte>>
  {
    [AlgorithmParameter]
    public byte Hue { get; set; }

    [AlgorithmParameter]
    public byte Saturation { get; set; }

    public override void Run()
    {
      var h = new Image<byte>(InputImage.Width, InputImage.Height, Hue);
      var s = new Image<byte>(InputImage.Width, InputImage.Height, Saturation);
      Result = new HslImage<byte>(h, s, InputImage).ToByteRgbImage();
    }
  }
}
